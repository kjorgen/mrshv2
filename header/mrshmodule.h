/*
 * mrshmodule.h
 *
 *  Created on: 9 May 2018
 *      Author: frenetic
 */

#ifndef HEADER_MRSHMODULE_H_
#define HEADER_MRSHMODULE_H_

/*
 * Python initialisation function
 */
PyMODINIT_FUNC initmrsh (void);

/*
 * Python interface functions
 */
static PyObject * mrshv2_hexdigest_buffer (PyObject *self, PyObject *args);
static PyObject * mrshv2_hexdigest(PyObject *self, PyObject *args);
static PyObject * mrshv2_diff (PyObject *self, PyObject *args);

/*
 * TODO: Remove from header
 */

char * generateFingerPrint (const char * path);

#endif /* HEADER_MRSHMODULE_H_ */
