#
# Standard template for distutils to build the python mrsh
# module extension
#
from distutils.core import setup, Extension

module1 = Extension('mrsh', sources = [	'src/bloomfilter.c',
					'src/fingerprint.c',
					'src/fingerprintList.c',
					'src/hashing.c',
					'src/main.c',
					'src/helper.c',
					'src/util.c',
					'src/test.c',
					'src/mrshmodule.c'
])

setup (name = 'mrsh',
       version = '1.0',
       description = 'Testing compile of shared object',
       ext_modules = [module1])

