# mrshv2

mrshv2 original source code available from:

http://www.fbreitinger.de/wp-content/uploads/2015/06/mrsh_v2.0.zip

This implementation has slightly changed the original source to include
a python interface to the mrshv2 algorithm so it can be incorporated into
the fuzzyhashlib library in support of kajura.

# Building different incarnations
## Build Python interface module
The following command will generate mrsh.so shared library in a build
subdirectory.

python setup.py build

## Build standalone executable
make

## Install into Python environment
pip install .

## Install into environment as an egg link to ease development
pip install -e .

Usage example:

import fuzzyhashlib

with open('/bin/ntfscat', 'r') as content_file:
    content = content_file.read()

a = fuzzyhashlib.mrshv2(buf=content)

with open('/bin/ntfswipe', 'r') as content_file:
    content = content_file.read()

b = fuzzyhashlib.mrshv2(buf=content)

print '%d' % (a - b)

