/*
 * mrshmodule.c
 *
 *  Created on: 9 May 2018
 *      Author: frenetic
 *
 * Compile using:
 *   pip install -e mrsh from outer layer project directory.
 */

#include <python2.7/Python.h>

#include <stdio.h>

#include "../header/main.h"
#include "../header/config.h"
#include "../header/fingerprint.h"
#include "../header/fingerprintList.h"

#include "../header/test.h"

#include "../header/mrshmodule.h"

/*
 * Custom error object we own and control.
 * note: the staticness in this module.
 */
static PyObject *MrshError;

/*
 * Setup python access to our C interface
 */
static PyMethodDef MrshMethods[] =
{
	{"hexdigest_buffer", mrshv2_hexdigest_buffer, METH_VARARGS, "Hash buffer into hexdigest."},
    {"hexdigest", mrshv2_hexdigest, METH_VARARGS, "Hash file/path into hexdigest."},
	{"diff", mrshv2_diff, METH_VARARGS, "Calculate the difference between two hashes."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};





/*
 * This is the first function the python interpreter calls
 * NULL returns means things went bad.
 */
PyMODINIT_FUNC initmrsh (void)
{
    PyObject *m;

    /*
     * This function creates the python module dictionary for us.
     */
    m = Py_InitModule("mrsh", MrshMethods);
    if (m == NULL)
        return;

    /*
     * Initialize the error as a python object
     */
    MrshError = PyErr_NewException("mrsh.error", NULL, NULL);

    /*
     * Increment the reference so the memory wont be deleted
     */
    Py_INCREF(MrshError);

    /*
     * Setup the global configuration instance located in main.c
     */
    initalizeDefaultModes ();

    /*
     * Add it to the module dictionary
     */
    PyModule_AddObject(m, "error", MrshError);
}


/*
 * Extract the hash as a string into allocated memory.
 *
 * Parameters:
 * 		fp : Fingerprint to turn into a stringified hash
 *
 * Return:
 * 		hash in string form
 *
 * NOTE:
 * 		This function allocates memory. Caller expected to free.
 */
char * fingerprintToString (const FINGERPRINT * fp)
{
	BLOOMFILTER * bf = 0;
	char * allocString = 0;
	int currentIndex = 0;
	char metadata[512];

	/*
	 * FORMAT: filename:filesize:number of filters:blocks in last filter
	 */
	sprintf (metadata,
			"%s:%d:%d:%d:"
			"",
			fp->file_name,
			fp->filesize,
			fp->amount_of_BF,
			fp->bf_list_last_element->amount_of_blocks
	);

	allocString =
			(char *) malloc (
				strlen (metadata) +
				2 * FILTERSIZE * (fp->amount_of_BF + 1) +
				1);

	strcpy (allocString, metadata);

	currentIndex = strlen (allocString);
	bf = fp->bf_list;

	while (bf != NULL)
	{
		for (int index = 0; index < FILTERSIZE; index++)
		{
			sprintf (allocString + currentIndex, "%02X", bf->array[index]);
			currentIndex += 2;
		}

		bf = bf->next;
	}

	allocString[currentIndex] = 0;
	return allocString;
}


/*
 * Rebuild a fingerprint object from the given hash
 */
FINGERPRINT * fingerprintFromString (char * hash)
{
	FINGERPRINT * fp = 0;
	char delims[] = ":";

	char * hex_string = 0;
	char * tokenize = 0;

	int blocks_in_last_bf = 0;
	int amount_of_BF = 0;

	/*
	 * 2 characters per actual hex byte.
	 */
	unsigned char *hex = (unsigned char *) malloc (FILTERSIZE * 2 + 1);

	fp = init_empty_fingerprint ();

	tokenize = strtok (hash, delims);

	int counter = 0;

	while (tokenize != NULL)
	{
		/*
		 * tokenize inserts null terminators into the original string.
		 * Repair tokenize damage on the run.
		 */
		switch (counter)
		{
			case 0:
				/*get the filename*/
				strcpy (fp->file_name, tokenize);
				tokenize[strlen(tokenize)] = ':';
				break;

			case 1:
				/*get the filesize*/
				fp->filesize = atoi (tokenize);
				tokenize[strlen(tokenize)] = ':';
				break;

			case 2:
				/*get the count of the filters*/
				amount_of_BF = atoi (tokenize);
				tokenize[strlen(tokenize)] = ':';
				break;

			case 3:
				/* only the last block of the filter have less than max blocks*/
				blocks_in_last_bf = atoi (tokenize);
				tokenize[strlen(tokenize)] = ':';
				break;

			case 4:
				/* hex_string is then the fingerprint */
				hex_string = (char *) malloc ((strlen (tokenize) + 1));

				strcpy (hex_string, tokenize);
				break;

			default:
				fprintf (stderr,
						"[*] ERROR IN PARSING FILE CONTENT OF HASH FILE");
				break;
		}

		tokenize = strtok (NULL, delims);
		counter++;
	}

	if (hex_string != NULL)
	{
		/*
		 * Reset bf_list when we read in a LIST
		 */
		fp->bf_list = NULL;
		fp->bf_list_last_element = NULL;

		for (int i = 0; i < amount_of_BF; i++)
		{
			/*
			 * Create a Bloom filter and add it to the fingerprint
			 */
			BLOOMFILTER *bf = init_empty_BF ();
			add_new_bloomfilter (fp, bf);
			memcpy (hex, &hex_string[FILTERSIZE * 2 * i], FILTERSIZE * 2);
			convert_hex_binary (hex, bf);
			bf->amount_of_blocks = MAXBLOCKS;
		}

		/*
		 * Get the last bit of the data
		 */
		BLOOMFILTER *bf = init_empty_BF ();
		add_new_bloomfilter (fp, bf);
		memcpy (hex, &hex_string[FILTERSIZE * 2 * amount_of_BF], FILTERSIZE * 2);
		convert_hex_binary (hex, bf);

		/*
		 * Update the final bloomfilter
		 */
		fp->bf_list_last_element = bf;
		fp->bf_list_last_element->amount_of_blocks = blocks_in_last_bf;

		free (hex_string);
		hex_string = NULL;
	}

	return fp;
}

/*
 * Generate and return the hash for the given path.
 *
 * Parameters:
 * 		path : A path to a single file in the filesystem
 *
 * Return:
 * 		Hash as an ascii string in allocated memory.
 */
char * generateFingerPrint (const char * path)
{
	FINGERPRINT_LIST *fpl = 0;
	char * allocString = 0;

	/*
	 * Best to be sure we actually have a string.
	 */
	if (!path)
		return NULL;

	fpl = init_empty_fingerprintList ();
	addPathToFingerprintList (fpl, path);
	allocString = fingerprintToString (fpl->list);
	fingerprintList_destroy (fpl);

	return allocString;
}

char * generateFingerPrintFromBuffer (const char * name, const char * buffer, unsigned int length)
{
	FINGERPRINT * fp = 0;
	char * allocString = 0;

	/*
	 * Best to be sure we actually have a string.
	 */
	if (!name || !buffer)
		return NULL;

	fp = init_fingerprint_for_buffer (name, buffer, length);
	allocString = fingerprintToString (fp);
	fingerprint_destroy (fp);

	return allocString;
}




static PyObject * mrshv2_diff (PyObject *self, PyObject *args)
{
	FINGERPRINT * fp1 = 0;
	FINGERPRINT * fp2 = 0;
	char * hash1 = 0;
	char * hash2 = 0;
	int score = 0;

	/*
	 * Convert the python string pointer to c ptr.
	 */
	if (!PyArg_ParseTuple(args, "ss", &hash1, &hash2))
		return NULL;

	/*
	 * Build the finger prints.
	 */
	fp1 = fingerprintFromString (hash1);
	fp2 = fingerprintFromString (hash2);

	score = fingerprint_compare(fp1, fp2);

	fingerprint_destroy (fp1);
	fingerprint_destroy (fp2);

	return Py_BuildValue("i", score);
}



/*
 * Function Name: mrshv2_hashfile
 *
 * Description:
 * 		Generate a hash from
 *
 * self => Can ignore this
 *
 * We want to first off interface the -p argument to generate hashes for files
 *
 */
static PyObject * mrshv2_hexdigest (PyObject *self, PyObject *args)
{
    const char *filename = 0;

    /*
     * Convert the python string pointer to c ptr.
     */
    if (!PyArg_ParseTuple(args, "s", &filename))
        return NULL;

    char * hash = generateFingerPrint (filename);

    PyObject * pythonString = Py_BuildValue("s", hash);
    free (hash);

    return pythonString;
}

static PyObject * mrshv2_hexdigest_buffer (PyObject *self, PyObject *args)
{
	PyByteArrayObject *bytearray = 0;
    const char *name = 0;
    unsigned char *buffer = 0;
    unsigned int length = 0;

    if (!PyArg_ParseTuple(args, "sOi", &name, &bytearray, &length))
		return NULL;

	buffer = PyByteArray_AsString((PyObject*) bytearray);
	char * hash = generateFingerPrintFromBuffer (name, buffer, length);

    PyObject * pythonString = Py_BuildValue("s", hash);
    free (hash);

    return pythonString;
}











