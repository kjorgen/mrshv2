/*
 * test.c
 *
 *  Created on: 11 May 2018
 *      Author: frenetic
 */

#include <python2.7/Python.h>

#include <stdio.h>

#include "../header/main.h"
#include "../header/config.h"
#include "../header/fingerprint.h"
#include "../header/fingerprintList.h"
#include "../header/mrshmodule.h"

#include "../header/test.h"

/*
 * Test function
 */
void kxjPrintFingerPrint (FINGERPRINT * fp1)
{
	printf ("Fingerprint structure details:\n"
			"\tbf_list = 0x%x\n"
			"\tbf_list_last_element = 0x%x\n"
			"\tnext = 0x%x\n"
			"\tamount_of_BF = %u\n"
			"\tfile_name = %s\n"
			"\tfilesize = %u\n"
			"\n",
			fp1->bf_list,
			fp1->bf_list_last_element,
			fp1->next,
			fp1->amount_of_BF,
			fp1->file_name,
			fp1->filesize
			);

	BLOOMFILTER * tmp = fp1->bf_list;

	int index = 0;
	while (tmp)
	{
		printf ("\tBloomfilter: %d\n"
				"\t\tamount_of_blocks = %hd\n"
				"\t\tnext = 0x%x\n"
				"\n",
				index,
				tmp->amount_of_blocks,
				tmp->next
		);

		for (int arrayIndex = 0; arrayIndex < FILTERSIZE; arrayIndex++)
		{
			printf ("%02X ", tmp->array[arrayIndex]);
		}

		printf ("\n\n");

		tmp = tmp->next;
		++index;
	}
}
